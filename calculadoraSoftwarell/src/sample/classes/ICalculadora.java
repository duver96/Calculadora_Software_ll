package sample.classes;

public interface ICalculadora <T>{
    public T sumar(T uno, T dos);
    public T restar(T uno, T dos);
    public T multiplicar(T uno, T dos);
    public T dividir(T uno, T dos);
}
