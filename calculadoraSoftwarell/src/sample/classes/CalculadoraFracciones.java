package sample.classes;

public class CalculadoraFracciones implements ICalculadora<Fraccion> {

    @Override
    public Fraccion sumar(Fraccion fraccion1, Fraccion fraccion2){
        Fraccion fraccionResultado;
        fraccionResultado = new Fraccion();
        double resultado1;
        double resultado2;
        resultado1 = fraccion1.getNumerador()*fraccion2.getDenominador();
        resultado2 =fraccion1.getDenominador()*fraccion2.getNumerador();
        fraccionResultado.setNumerador(resultado1+resultado2);
        fraccionResultado.setDenominador(fraccion1.getDenominador()*fraccion2.getDenominador());
        //return fraccionResultado;
        return simplificar(fraccionResultado);
    }

    @Override
    public Fraccion restar(Fraccion fraccion1, Fraccion fraccion2){
        Fraccion fraccionResultado;
        fraccionResultado = new Fraccion();
        double resultado1;
        double resultado2;
        resultado1 = fraccion1.getNumerador()*fraccion2.getDenominador();
        resultado2 =fraccion1.getDenominador()*fraccion2.getNumerador();
        fraccionResultado.setNumerador(resultado1-resultado2);
        fraccionResultado.setDenominador(fraccion1.getDenominador()*fraccion2.getDenominador());
        //return fraccionResultado;
        return simplificar(fraccionResultado);
    }

    @Override
    public Fraccion multiplicar(Fraccion fraccion1, Fraccion fraccion2){
        Fraccion fraccionResultado;
        fraccionResultado = new Fraccion();
        fraccionResultado.setNumerador(fraccion1.getNumerador()*fraccion2.getNumerador());
        fraccionResultado.setDenominador(fraccion1.getDenominador()*fraccion2.getDenominador());

        return simplificar(fraccionResultado);
    }
    @Override
    public Fraccion dividir(Fraccion fraccion1, Fraccion fraccion2){
        Fraccion fraccionResultado;
        fraccionResultado = new Fraccion();
        fraccionResultado.setNumerador(fraccion1.getNumerador()*fraccion2.getDenominador());
        fraccionResultado.setDenominador(fraccion1.getDenominador()*fraccion2.getNumerador());

        return simplificar(fraccionResultado);
    }
    //el metodo simplificr nos ayudara a que las fracciones queden lo mas simplidicado posible
    public Fraccion simplificar(Fraccion fraccionASimplificar){
        int cont = 2;
        while (cont <= Math.abs(fraccionASimplificar.getNumerador()) && cont <= Math.abs(fraccionASimplificar.getDenominador())) {
            if (fraccionASimplificar.getNumerador() % cont == 0 && fraccionASimplificar.getDenominador() % cont == 0) {
                fraccionASimplificar.setNumerador(fraccionASimplificar.getNumerador()/ cont);
                fraccionASimplificar.setDenominador(fraccionASimplificar.getDenominador()/ cont);

            }
            else
                cont++;
        }
        return  fraccionASimplificar;
    }

}
