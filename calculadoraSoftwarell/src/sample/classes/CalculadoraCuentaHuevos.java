package sample.classes;

public class CalculadoraCuentaHuevos implements ICalculadora <Double>{


    @Override
    public Double sumar(Double uno, Double dos) {
        return uno+dos;
    }

    @Override
    public Double restar(Double uno, Double dos) {
        return uno-dos;
    }

    @Override
    public Double multiplicar(Double uno, Double dos) {
        return uno*dos;
    }

    @Override
    public Double dividir(Double uno, Double dos) {
        return uno/dos;
    }

    //la clase operaciones nos ayudara a realizar las operaciones en nuestra calculadora

    public boolean mayorque(Double uno, Double dos){
        if(uno>dos){
            return true;
        }else
            return false;
    }
    public boolean menorque(Double uno, Double dos) {
        if (uno < dos) {
            return true;
        } else
            return false;
    }
    public boolean mayorigual(Double uno, Double dos){
        if(uno>=dos){
            return true;
        }else
            return false;
    }
    public boolean menorigual(Double uno, Double dos){
        if(uno<=dos){
            return true;
        }else
            return false;
    }
    public boolean igual (Double uno, Double dos){
        if(uno==dos){
            return true;
        }else
            return false;
    }
    public double raiz(Double uno, Double dos){
        return Math.pow(uno,1/dos);
    }
    public double potencia(Double uno, Double dos){
        return Math.pow(uno,dos);
    }

    public boolean entero(Double numero1){
        if (numero1%1==0){
            return true;
        }else
            return false;
    }


}
