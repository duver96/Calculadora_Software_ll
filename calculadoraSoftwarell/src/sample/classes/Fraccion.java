package sample.classes;

import javax.swing.*;

public class Fraccion {
    private double numerador;
    private double denominador;
    //se crea la clase franccion para manejar las operaciones entre fracciones
    public Fraccion() {
    }
    //la fraccion se compone de un numerador y un denominador
    public Fraccion(double numerador, double denominador) {
        if (this.denominador==0){
            JOptionPane.showMessageDialog(null,"division por 0 no permitida");

        }else{
            this.numerador = numerador;
            this.denominador = denominador;
        }

    }

    public double getDenominador() {
        return denominador;
    }

    public void setDenominador(double denominador){
        this.denominador = denominador;
    }

    public double getNumerador() {
        return numerador;
    }

    public void setNumerador(double numerador) {
        this.numerador = numerador;
    }



    //se crean las operaciones entre fracciones


}
