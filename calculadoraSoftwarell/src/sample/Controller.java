package sample;

import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import controller.AuditoriaController;
import controller.UserController;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import model.Auditoria;
import model.User;
import sample.classes.CalculadoraCuentaHuevos;
import sample.classes.CalculadoraFracciones;
import sample.classes.Fraccion;

import javax.swing.*;
import java.net.URL;
import java.util.Date;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    //paneles
    @FXML private AnchorPane startPanel;
    @FXML private AnchorPane enterPanel;
    @FXML private AnchorPane registerPanel;
    @FXML private AnchorPane calculator;
    @FXML private AnchorPane fracciones;

    //JFXTextFields
    @FXML private JFXTextField enterUserName;

    @FXML private JFXTextField registerUserName;
    @FXML private JFXTextField numbers;
    @FXML private JFXTextField denominador;
    @FXML private JFXTextField numerador;

    //JFXPasswordFields
    @FXML private JFXPasswordField enterPassword;
    @FXML private JFXPasswordField registerPassword;

    AuditoriaController auditoriaController = new AuditoriaController();

    //Fechas
    java.util.Date fechaIngreso;
    java.util.Date fechaSalio;

    CalculadoraCuentaHuevos calculadoraCuentaHuevos = new CalculadoraCuentaHuevos();
    CalculadoraFracciones calculadoraFracciones = new CalculadoraFracciones();
    boolean primerUso = false;
    double resultado;
    double calculadoraCuentaHuevos1;
    double calculadoraCuentaHuevos2;
    boolean operacionDeMayor = false;
    int intentosErroneosDeIngreso =0;

    String operacionARealizarFraccion;
    String operacionARealizar;
    String usuarioLogueado;

    Fraccion fraccion1 = new Fraccion();
    Fraccion fraccion2= new Fraccion();
    Fraccion fraccionResultado= new Fraccion();


    public void initialize(URL location, ResourceBundle resources) {
        //asignamos el filtro creado en el metodo siguiente al textfield numbers

        numbers.addEventFilter(KeyEvent.ANY, filtroCamposNumericos);
        denominador.addEventFilter(KeyEvent.ANY, filtroCamposNumericos);
        numerador.addEventFilter(KeyEvent.ANY, filtroCamposNumericos);


    }

    EventHandler<KeyEvent> filtroCamposNumericos = new EventHandler<KeyEvent>() {
        private boolean willConsume = false;
        @Override
        public void handle(KeyEvent event) {
            JFXTextField temp = (JFXTextField) event.getSource();

            if(willConsume){
                event.consume();
            }

            if(!event.getCode().isDigitKey() && event.getCode() != KeyCode.BACK_SPACE && event.getCode() != KeyCode.SHIFT){
                if(event.getEventType()== KeyEvent.KEY_PRESSED){
                    willConsume=true;
                }
                else if(event.getEventType() == KeyEvent.KEY_RELEASED){
                    willConsume= false;
                }
            }

        }
    };

    public void onBackToCalculator(){
        fracciones.setVisible(false);
        numerador.setText("");
        denominador.setText("");
        calculator.setVisible(true);
    }

    public void onRegisterButtonClicked (MouseEvent event){
        startPanel.setVisible(false);
        registerPanel.setVisible(true);
    }

    public void onEnterButtonClicked (MouseEvent event){
        startPanel.setVisible(false);
        enterPanel.setVisible(true);
    }

    public void onBackButtonClicked (MouseEvent event){
        startPanel.setVisible(true);
        enterPanel.setVisible(false);
        registerPanel.setVisible(false);
    }

    public void onExitButtonClicked(MouseEvent event){
        fechaSalio = new Date();
        Auditoria auditoria = new Auditoria(0,intentosErroneosDeIngreso,usuarioLogueado,fechaIngreso,fechaSalio);
        auditoriaController.registrar(auditoria);
        Platform.exit();
        System.exit(0);
    }

    public void onToEnterButtonClicked(MouseEvent event){
        String userName;
        String password;

        userName = enterUserName.getText().trim();
        password = enterPassword.getText();
        usuarioLogueado = userName;

        boolean userRegistered;
        userRegistered = false;

        UserController userController = new UserController();

        for (User user: userController.verClientes() ) {
            if (user.getUserName().equals(userName) && user.getPassword().equals(password)){
                userRegistered = true;
                enterUserName.clear();
                enterPassword.clear();
                JOptionPane.showMessageDialog(null,"usuario logueado, ya puede usar la calculadora");
                fechaIngreso = new Date();
                enterPanel.setVisible(false);
                calculator.setVisible(true);
                fechaIngreso = new Date();
                break;
            }

        }
        if (!userRegistered) {
            JOptionPane.showMessageDialog(null,"Usuario Inexistente, o contraseña incorrecta. Revise los datos ingresados");
            intentosErroneosDeIngreso +=1;
        }

    }

    public void onToRegisterButtonClicked(MouseEvent event){
        String userName;
        String password;

        userName = registerUserName.getText().trim();
        password = registerPassword.getText();


        boolean userRegistered;
        userRegistered = false;

        UserController userController = new UserController();

        for (User user: userController.verClientes() ) {
            if (user.getUserName().equals(userName)){
                userRegistered = true;
                break;
            }
        }
        if (userRegistered){
            registerUserName.clear();
            registerPassword.clear();
            JOptionPane.showMessageDialog(null,"usuario existente, intente con otro nombre de usuario");

        }
        else{
            if(registerPassword.getText().isEmpty() || registerUserName.getText().trim().isEmpty()){
                JOptionPane.showMessageDialog(null,"Debes llenar todos los campos");
            }
            else{
                User user = new User(0,userName,password);
                userController.registrar(user);
                JOptionPane.showMessageDialog(null,"Usuario Registrado, ahora puede iniciar sesion");
                registerPanel.setVisible(false);
                enterUserName.clear();
                enterPassword.clear();
                enterPanel.setVisible(true);
            }
            registerUserName.clear();
            registerPassword.clear();

        }






    }
    public void onOneButtonClicked(MouseEvent event){
        numbers.setText(numbers.getText() + "1");

    }

    public void onTwoButtonClicked(MouseEvent event){
        numbers.setText(numbers.getText()+"2");

    }

    public void onThreeButtonClicked(MouseEvent event){
        numbers.setText(numbers.getText()+"3");

    }

    public void onFourButtonClicked(MouseEvent event){
        numbers.setText(numbers.getText()+"4");

    }

    public void onFiveButtonClicked(MouseEvent event){
        numbers.setText(numbers.getText()+"5");
    }

    public void onSixButtonClicked(MouseEvent event){
        numbers.setText(numbers.getText()+"6");

    }

    public void onSevenButtonClicked(MouseEvent event){
        numbers.setText(numbers.getText()+"7");

    }

    public void onEightButtonClicked(MouseEvent event){
        numbers.setText(numbers.getText()+"8");

    }

    public void onNineButtonClicked(MouseEvent event){
        numbers.setText(numbers.getText()+"9");

    }
    public void onZeroButtonClicked(MouseEvent event){
        numbers.setText(numbers.getText()+"0");

    }

    public void onMasButtonClicked(MouseEvent  event){
        if(!primerUso&&numbers.getText().length()!=0){
            calculadoraCuentaHuevos1 = Double.parseDouble(numbers.getText());
            numbers.clear();
            primerUso =true;
            operacionARealizar = "+";
        }
        else{
            mostrarMensajeParaIngresarOtroNumero();

        }


    }

    public void restartValues(){
        primerUso = false;
        calculadoraCuentaHuevos1 =0;
        calculadoraCuentaHuevos2 =0;
        numbers.clear();
        denominador.clear();
        numerador.clear();
        fraccion1.setDenominador(0);
        fraccion2.setDenominador(0);
        fraccion1.setNumerador(0);
        fraccion2.setNumerador(0);
        fraccionResultado.setNumerador(0);
        fraccionResultado.setDenominador(0);
    }
    public void onMenosButtonClicked(MouseEvent event){
        if(!primerUso&&numbers.getText().length()!=0){
            calculadoraCuentaHuevos1 = Double.parseDouble(numbers.getText());
            numbers.clear();
            primerUso =true;
            operacionARealizar = "-";
        }
        else{
            mostrarMensajeParaIngresarOtroNumero();


        }

    }


    public void onPorButtonClicked(MouseEvent event){
        if(!primerUso&&numbers.getText().length()!=0){
            calculadoraCuentaHuevos1 = Double.parseDouble(numbers.getText());
            numbers.clear();
            primerUso =true;
            operacionARealizar = "*";

        }
        else{
            mostrarMensajeParaIngresarOtroNumero();
        }

    }


    public void onMayorButtonClicked(MouseEvent event) {
        if (!primerUso&&numbers.getText().length()!=0) {
            calculadoraCuentaHuevos1 = Double.parseDouble(numbers.getText());
            numbers.clear();
            primerUso = true;
            operacionARealizar =">";
            operacionDeMayor = true;

        } else {
            mostrarMensajeParaIngresarOtroNumero();


        }
    }

    public void onMayorOIgualButtonClicked (MouseEvent event){
        if (!primerUso&&numbers.getText().length()!=0) {
            calculadoraCuentaHuevos1 = Double.parseDouble(numbers.getText());
            numbers.clear();
            primerUso = true;
            operacionARealizar =">=";
            operacionDeMayor = true;

        } else {
            mostrarMensajeParaIngresarOtroNumero();
        }


    }
    public void onDivisionButtonClicked (MouseEvent event){
        if (!primerUso&&numbers.getText().length()!=0) {
            calculadoraCuentaHuevos1 = Double.parseDouble(numbers.getText());
            numbers.clear();
            primerUso = true;
            operacionARealizar ="/";

        } else {
            mostrarMensajeParaIngresarOtroNumero();
        }


    }

    public void onMenorButtonClicked (MouseEvent event){
        if (!primerUso&&numbers.getText().length()!=0) {
            calculadoraCuentaHuevos1 = Double.parseDouble(numbers.getText());
            numbers.clear();
            primerUso = true;
            operacionARealizar ="<";
            operacionDeMayor = true;

        } else {
            mostrarMensajeParaIngresarOtroNumero();
        }


    }

    public void onMenorOIgualButtonClicked (MouseEvent event){
        if (!primerUso&&numbers.getText().length()!=0) {
            calculadoraCuentaHuevos1 = Double.parseDouble(numbers.getText());
            numbers.clear();
            primerUso = true;
            operacionARealizar ="<=";
            operacionDeMayor = true;

        } else {
            mostrarMensajeParaIngresarOtroNumero();


        }
    }
    public void onPotenciaButtonClicked (MouseEvent event){
        if (!primerUso &&numbers.getText().length()!=0) {
            calculadoraCuentaHuevos1 = Double.parseDouble(numbers.getText());
            numbers.clear();
            primerUso = true;
            operacionARealizar ="**";

        } else {
            mostrarMensajeParaIngresarOtroNumero();
        }
    }

    public void onRaizButtonClicked(MouseEvent event){
        if (!primerUso &&numbers.getText().length()!=0) {
            calculadoraCuentaHuevos1 = Double.parseDouble(numbers.getText());
            numbers.clear();
            primerUso = true;
            operacionARealizar ="raiz";

        } else {
            mostrarMensajeParaIngresarOtroNumero();
        }

    }

    public void onEqualsButtonClicked(MouseEvent event){
        if (!primerUso && numbers.getText().length()!=0) {
            calculadoraCuentaHuevos1 = Double.parseDouble(numbers.getText());
            numbers.clear();
            primerUso = true;
            operacionARealizar ="=";
            operacionDeMayor = true;

        } else {

            mostrarMensajeParaIngresarOtroNumero();
        }

    }

    public void onFraccionesButtonClicked(MouseEvent event){
        restartValues();
        calculator.setVisible(false);
        fracciones.setVisible(true);

    }

    public  void llamarOperacion(double calculadoraCuentaHuevos1,double calculadoraCuentaHuevos2, String operacion){
        switch (operacion){
            case "+":
                resultado= calculadoraCuentaHuevos.sumar(calculadoraCuentaHuevos1,calculadoraCuentaHuevos2);
                break;
            case "-":
                resultado= calculadoraCuentaHuevos.restar(calculadoraCuentaHuevos1,calculadoraCuentaHuevos2);
                break;
            case "*":
                resultado= calculadoraCuentaHuevos.multiplicar(calculadoraCuentaHuevos1,calculadoraCuentaHuevos2);
                break;
            case "/":
                resultado= calculadoraCuentaHuevos.dividir(calculadoraCuentaHuevos1,calculadoraCuentaHuevos2);
                break;
            case "**":
                resultado= calculadoraCuentaHuevos.potencia(calculadoraCuentaHuevos1,calculadoraCuentaHuevos2);
                break;
            case "raiz":
                resultado = calculadoraCuentaHuevos.raiz(calculadoraCuentaHuevos1,calculadoraCuentaHuevos2);
                break;
            case ">":
                if(calculadoraCuentaHuevos.mayorque(calculadoraCuentaHuevos1,calculadoraCuentaHuevos2))
                    JOptionPane.showMessageDialog(null,"EL calculadoraCuentaHuevos1 es mayor al calculadoraCuentaHuevos 2");
                else
                    JOptionPane.showMessageDialog(null,"EL calculadoraCuentaHuevos1 no es mayor al calculadoraCuentaHuevos 2");
                break;
            case ">=":
                if(calculadoraCuentaHuevos.mayorigual(calculadoraCuentaHuevos1,calculadoraCuentaHuevos2))
                    JOptionPane.showMessageDialog(null,"EL calculadoraCuentaHuevos1 es mayor o igual al calculadoraCuentaHuevos 2");
                else
                    JOptionPane.showMessageDialog(null,"EL calculadoraCuentaHuevos1 no es mayor o igual al calculadoraCuentaHuevos 2");
                break;
            case "<":
                if(calculadoraCuentaHuevos.menorque(calculadoraCuentaHuevos1,calculadoraCuentaHuevos2))
                    JOptionPane.showMessageDialog(null,"EL calculadoraCuentaHuevos1 es menor al calculadoraCuentaHuevos 2");
                else
                    JOptionPane.showMessageDialog(null,"EL calculadoraCuentaHuevos1 no es menor al calculadoraCuentaHuevos 2");
                break;
            case "<=":
                if(calculadoraCuentaHuevos.menorigual(calculadoraCuentaHuevos1,calculadoraCuentaHuevos2))
                    JOptionPane.showMessageDialog(null,"EL calculadoraCuentaHuevos1 es menor o igual al calculadoraCuentaHuevos 2");
                else
                    JOptionPane.showMessageDialog(null,"EL calculadoraCuentaHuevos1 no es menor o igual al calculadoraCuentaHuevos 2");
                break;
            case"=":
                if(calculadoraCuentaHuevos.igual(calculadoraCuentaHuevos1,calculadoraCuentaHuevos2))
                    JOptionPane.showMessageDialog(null,"EL calculadoraCuentaHuevos1 es igual al calculadoraCuentaHuevos 2");
                else
                    JOptionPane.showMessageDialog(null,"EL calculadoraCuentaHuevos1 no es igual al calculadoraCuentaHuevos 2");
                break;




        }
    }

    public void onResultadoButtonClicked(MouseEvent event){
        if(primerUso){
            if(!numbers.getText().trim().isEmpty())
                calculadoraCuentaHuevos2 = Double.parseDouble(numbers.getText());

            if(!operacionDeMayor) {
                if(operacionARealizar.equals("/") && calculadoraCuentaHuevos2 ==0){
                    JOptionPane.showMessageDialog(null, "La dcvision por cero no esta definida");
                }
                else{
                    llamarOperacion(calculadoraCuentaHuevos1, calculadoraCuentaHuevos2, operacionARealizar);
                    JOptionPane.showMessageDialog(null, "El resultado es: " + resultado);

                }
                restartValues();
            }

            else{
                llamarOperacion(calculadoraCuentaHuevos1, calculadoraCuentaHuevos2, operacionARealizar);
                restartValues();
            }
            operacionDeMayor = false;

        }
        else{
            JOptionPane.showMessageDialog(null, "Debe ingresar dos numeros ");
        }
    }

    public void mostrarMensajeParaIngresarOtroNumero(){
        JOptionPane.showMessageDialog(null,"Debe ingresar dos numeros");
        restartValues();
    }

    public void onSumaFracciones(){
        if (!primerUso && numerador.getText().length()!=0  && denominador.getText().length()!=0 && !denominador.getText().equals("0") ) {
            fraccion1.setNumerador(Integer.parseInt(numerador.getText()));
            fraccion1.setDenominador(Integer.parseInt(denominador.getText()));
            numerador.clear();
            denominador.clear();
            primerUso = true;
            operacionARealizarFraccion ="+";
            System.out.print(fraccion1.getDenominador()+" "+fraccion1.getNumerador());

        } else {
            JOptionPane.showMessageDialog(null, "Debe ingresar todos los campos y el denomionador no puede ser 0");

        }


    }

    public void onPorFracciones(){
        if (!primerUso && numerador.getText().length()!=0 && denominador.getText().length()!=0  && !denominador.getText().equals("0")) {
            fraccion1.setNumerador(Integer.parseInt(numerador.getText()));
            fraccion1.setDenominador(Integer.parseInt(denominador.getText()));
            numerador.clear();
            denominador.clear();
            primerUso = true;
            operacionARealizarFraccion ="*";
            System.out.print(fraccion1.getDenominador()+" "+fraccion1.getNumerador());

        }else {
            JOptionPane.showMessageDialog(null, "Debe ingresar todos los campos y el denomionador no puede ser 0");

        }
    }
    public void onrestarFracciones(){
        if (!primerUso && numerador.getText().length()!=0 && denominador.getText().length()!=0&& !denominador.getText().equals("0")&& !denominador.getText().equals("0")) {
            fraccion1.setNumerador(Integer.parseInt(numerador.getText()));
            fraccion1.setDenominador(Integer.parseInt(denominador.getText()));
            numerador.clear();
            denominador.clear();
            primerUso = true;
            operacionARealizarFraccion ="-";
            System.out.print(fraccion1.getDenominador()+" "+fraccion1.getNumerador());

        }else {
            JOptionPane.showMessageDialog(null, "Debe ingresar todos los camposy el denomionador no puede ser 0");

        }
    }
    public void ondividirFracciones(){
        if (!primerUso && numerador.getText().length()!=0 && denominador.getText().length()!=0&& !denominador.getText().equals("0")) {
            fraccion1.setNumerador(Integer.parseInt(numerador.getText()));
            fraccion1.setDenominador(Integer.parseInt(denominador.getText()));
            numerador.clear();
            denominador.clear();
            primerUso = true;
            operacionARealizarFraccion ="/";
            System.out.print(fraccion1.getDenominador()+" "+fraccion1.getNumerador());

        }else {
            JOptionPane.showMessageDialog(null, "Debe ingresar todos los campos y el denomionador no puede ser 0");

        }
    }

    public void igualFraccion(){
        if(primerUso){
            if(Integer.parseInt(denominador.getText())!=0){
                fraccion2.setNumerador(Integer.parseInt(numerador.getText()));
                fraccion2.setDenominador(Integer.parseInt(denominador.getText()));
                llamarOperacionFraccion(operacionARealizarFraccion,fraccion1,fraccion2);
                if(fraccionResultado.getNumerador() ==0)
                    JOptionPane.showMessageDialog(null, "El resultado es: 0");

                else if(fraccionResultado.getDenominador()==1){
                    JOptionPane.showMessageDialog(null,"El resultado es:" + fraccionResultado.getNumerador());

                }
                else{
                    JOptionPane.showMessageDialog(null, "El resultado es: " + fraccionResultado.getNumerador() + "/" + fraccionResultado.getDenominador());
                }
                restartValues();

            }
            else
                JOptionPane.showMessageDialog(null,"Viejo, la division por 0 no esta definida");


        }
        else{
            JOptionPane.showMessageDialog(null, "Debe ingresar dos fracciones");
        }
        restartValues();
    }


    public void llamarOperacionFraccion(String operacionARealizarFracciones,Fraccion fraccion1, Fraccion fraccion2) {
        switch (operacionARealizarFracciones){

            case "+":
                fraccionResultado = calculadoraFracciones.sumar(fraccion1,fraccion2);

                break;
            case "-":
                fraccionResultado = calculadoraFracciones.restar(fraccion1,fraccion2);
                break;
            case "*":
                fraccionResultado = calculadoraFracciones.multiplicar(fraccion1,fraccion2);
                break;
            case "/":
                fraccionResultado = calculadoraFracciones.dividir(fraccion1,fraccion2);
                break;

        }

    }

    public void onSignoMenosClicked(MouseEvent event){
        if( /*numbers.isFocused()&&*/numbers.getText().length()==0 && !numbers.getText().contains("-")){
            numbers.setText(numbers.getText()+"-");
        }/*
        else if(numerador.isFocused()&&numerador.getText().length()==0 && !numerador.getText().contains("-")){
            numerador.setText(denominador.getText()+"-");
        }
        else if(denominador.isFocused()&&denominador.getText().length()==0 && !denominador.getText().contains("-")){
            denominador.setText(denominador.getText()+"-");
        }*/
    }

    public void onPuntoClicked(MouseEvent event){
        if (numbers.getText().contains(".")){

        }
        else
            numbers.setText(numbers.getText()+".");
    }

}


