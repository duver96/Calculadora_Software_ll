package dao;

import connection.Conexion;
import idao.IUserDao;
import model.User;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class UserDaoImpl implements IUserDao {
    @Override
    public boolean registrar(User user) {
        boolean registrar = false;

        Statement stm = null;
        Connection con = null;

        String sql = "INSERT INTO user values (NULL+'" + "','" + user.getUserName() + "','"
                + user.getPassword()+ "')";

        try {
            con = Conexion.conectar();
            stm = (Statement) con.createStatement();
            stm.execute(sql);
            registrar = true;
            stm.close();
            con.close();
        } catch (SQLException e) {
            System.out.println("Error: Clase UserDaoImpl, metodo registrar");
            System.out.println(sql);
            e.printStackTrace();
        }
        return registrar;
    }

    @Override
    public List<User> obtener() {
        Connection co = null;
        Statement stm = null;
        ResultSet rs = null;

        String sql = "SELECT * FROM user ";

        List<User> userList = new ArrayList<User>();

        try {
            co = Conexion.conectar();
            stm = (Statement) co.createStatement();
            rs = ((java.sql.Statement) stm).executeQuery(sql);
            while (rs.next()) {
                User user = new User();
                user.setUserIde(rs.getInt(1));
                user.setUserName(rs.getString(2));
                user.setPassword(rs.getString(3));
                userList.add(user);
            }
            stm.close();
            rs.close();
            co.close();
        } catch (SQLException e) {
            System.out.println("Error: Clase UserDaoImpl, metodo obtener");
            e.printStackTrace();
        }

        return userList;
    }

}
