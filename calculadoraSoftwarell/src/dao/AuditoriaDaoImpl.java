package dao;

import connection.Conexion;
import idao.IAuditoriaDao;
import model.Auditoria;
import model.User;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class AuditoriaDaoImpl implements IAuditoriaDao {

    @Override
    public boolean registrar(Auditoria auditoria) {
        boolean registrar = false;

        Statement stm = null;
        Connection con = null;

        String sql = "INSERT INTO auditoria values (NULL+'" + "','" + auditoria.getIntentosErroneosDeIngreso() + "','"
                + auditoria.getUsuarioQueIngreso()+"','" + auditoria.getHoraIngreso()+"','" + auditoria.getHoraFinalizo()+ "')";
        try {
            con = Conexion.conectar();
            stm = (Statement) con.createStatement();
            stm.execute(sql);
            registrar = true;
            stm.close();
            con.close();
        } catch (SQLException e) {
            System.out.println("Error: Clase AuditoriaDaoImpl, metodo registrar");
            System.out.println(sql);
            e.printStackTrace();
        }
        return registrar;
    }

    @Override
    public List<Auditoria> obtener() {
        Connection co = null;
        Statement stm = null;
        ResultSet rs = null;

        String sql = "SELECT * FROM auditoria ";

        List<Auditoria> auditoriaList = new ArrayList<Auditoria>();

        try {
            co = Conexion.conectar();
            stm = (Statement) co.createStatement();
            rs = ((java.sql.Statement) stm).executeQuery(sql);
            while (rs.next()) {
                Auditoria auditoria = new Auditoria();
                auditoria.setIdauditoria(rs.getInt(1));
                auditoria.setUsuarioQueIngreso(rs.getString(2));
                auditoria.setHoraIngreso(rs.getDate(3));
                auditoria.setHoraFinalizo(rs.getDate(3));
                auditoriaList.add(auditoria);
            }
            stm.close();
            rs.close();
            co.close();
        } catch (SQLException e) {
            System.out.println("Error: Clase AuditoriaDaoImpl, metodo obtener");
            e.printStackTrace();
        }

        return auditoriaList;
    }

}
