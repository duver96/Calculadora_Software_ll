package model;

import java.util.Date;

public class Auditoria {
    private int idauditoria;
    private int IntentosErroneosDeIngreso;
    private String usuarioQueIngreso;
    private Date horaIngreso;
    private Date horaFinalizo;

    public Auditoria() {

    }

    public Auditoria(int idauditoria, int IntentosErroneosDeIngreso, String usuarioQueIngreso, Date horaIngreso, Date horaFinalizo) {
        this.idauditoria = idauditoria;
        this.IntentosErroneosDeIngreso = IntentosErroneosDeIngreso;
        this.usuarioQueIngreso = usuarioQueIngreso;
        this.horaIngreso = horaIngreso;
        this.horaFinalizo = horaFinalizo;
    }

    public int getIdauditoria() {
        return idauditoria;
    }

    public void setIdauditoria(int idauditoria) {
        this.idauditoria = idauditoria;
    }

    public int getIntentosErroneosDeIngreso() {
        return IntentosErroneosDeIngreso;
    }

    public void setIntentosErroneosDeIngreso(int intentosErroneosDeIngreso) {
        IntentosErroneosDeIngreso = intentosErroneosDeIngreso;
    }

    public String getUsuarioQueIngreso() {
        return usuarioQueIngreso;
    }

    public void setUsuarioQueIngreso(String usuarioQueIngreso) {
        this.usuarioQueIngreso = usuarioQueIngreso;
    }

    public Date getHoraIngreso() {
        return horaIngreso;
    }

    public void setHoraIngreso(Date horaIngreso) {
        this.horaIngreso = horaIngreso;
    }

    public Date getHoraFinalizo() {
        return horaFinalizo;
    }

    public void setHoraFinalizo(Date horaFinalizo) {
        this.horaFinalizo = horaFinalizo;
    }
}
