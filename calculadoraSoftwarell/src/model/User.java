package model;

public class User {

    private int userIde;
    private String userName;
    private String password;

    public User() {
    }

    public User(int userIde, String userName, String password) {
        this.userIde = userIde;
        this.userName = userName;
        this.password = password;
    }

    public int getUserIde() {
        return userIde;
    }

    public void setUserIde(int userIde) {
        this.userIde = userIde;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
