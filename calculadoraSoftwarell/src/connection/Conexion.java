package connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexion {

    public static Connection conectar() {
        Connection conexion = null;

        String password = "1234";
        String user = "root";
        try {

            // una sola cadena de conexión, en un sólo parámetro se concatena el
            // usuario y el password


            String url = "jdbc:mysql://localhost/mydb?useUnicode=true"
                    +"&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false"
                    +"&serverTimezone=UTC&useSSL=false";

            conexion = DriverManager.getConnection(url,user,password);
            if (conexion != null) {
                System.out.println("Conexión satisfactoria");
            }

        } catch (SQLException e) {
            System.out.println(
                    "Error en la conexión");
            e.printStackTrace();
        }

        return conexion;
    }
       /* Connection con = null;

        String password = "1234";
        String usuario = "root";
        String url = "jdbc:mysql://localhost:3306/MyParking?user=" + usuario
                + "&password=" + password;
        try {
            con = DriverManager.getConnection(url);
            if (con != null) {
                System.out.println("Conectado");
            }
        } catch (SQLException e) {
            System.out.println("No se pudo conectar a la base de datos");
            e.printStackTrace();
        }
        return con;
    }*/



}
