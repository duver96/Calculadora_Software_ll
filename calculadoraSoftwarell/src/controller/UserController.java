package controller;

import dao.UserDaoImpl;
import idao.IUserDao;
import model.User;

import java.util.ArrayList;
import java.util.List;

public class UserController {
    //llama al DAO para guardar un cliente
    public void registrar(User user ) {
        IUserDao dao=  new UserDaoImpl();
        dao.registrar(user);
    }


    //llama al DAO para obtener todos los clientes y luego los muestra en la vista
    public List<User>  verClientes(){
        List<User> users = new ArrayList<User>();
        IUserDao dao=  new UserDaoImpl();
        users=dao.obtener();
        return users;
        //vista.verClientes(clientes);
    }


}
