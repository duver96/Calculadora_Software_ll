package controller;

import dao.AuditoriaDaoImpl;
import dao.UserDaoImpl;
import idao.IAuditoriaDao;
import idao.IUserDao;
import model.Auditoria;
import model.User;

import java.util.ArrayList;
import java.util.List;

public class AuditoriaController {

    public void registrar(Auditoria auditoria) {
        IAuditoriaDao dao=  new AuditoriaDaoImpl();
        dao.registrar(auditoria);
    }

}
