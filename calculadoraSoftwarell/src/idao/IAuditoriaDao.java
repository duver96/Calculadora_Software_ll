package idao;

import model.Auditoria;
import model.User;

import java.util.List;

public interface IAuditoriaDao {
    public boolean registrar(Auditoria auditoria);
    public List<Auditoria> obtener();
}
