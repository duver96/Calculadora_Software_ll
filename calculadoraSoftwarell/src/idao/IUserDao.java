package idao;

import model.User;

import java.util.List;

public interface IUserDao {


    public boolean registrar(User user);
    public List<User> obtener();

}
